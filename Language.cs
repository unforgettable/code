﻿using UnityEngine;


// Код смены языка и сохранения значения на устройстве пользователя для приложения на Unity.


public class Language : MonoBehaviour
{
    //Структура - Название(ключ), Название Английское, Название русское
    //Для изменения названий, добавления, удаления изменять данные в этом массиве 
    string[,] lgSetting = { 
                            {"BTN_play_in_menu", "Play", "Играть"},
                            {"BTN_setting_in_menu", "Setting", "Настройки"},
                            {"NoneName", "NomeENG", "NoneRus"}
                        //----------Сюда добавлять следубющие элементы в такой же последовательности

                        //---------------------------------------------
                          };
    void Start ()
    {
        for (int i = 0; i < lgSetting.Length; i++)
            PlayerPrefs.SetString(lgSetting[i, 0], PlayerPrefs.GetString(lgSetting[i, 0], lgSetting[i, 1]));
        PlayerPrefs.Save();
    }

	public void TranslateEng()
    {    //Для смены На английский
        for (int i = 0; i < lgSetting.Length; i++)
             PlayerPrefs.SetString(lgSetting[i, 0], lgSetting[i, 1]);
        PlayerPrefs.Save();
    }

    public void TranslateRus()
    {   //Для смены На русский
        for (int i = 0; i < lgSetting.Length; i++)
            PlayerPrefs.SetString(lgSetting[i, 0], lgSetting[i, 2]);
        PlayerPrefs.Save();
    }
}
