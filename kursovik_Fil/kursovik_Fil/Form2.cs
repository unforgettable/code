﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace kursovik_Fil
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream(@"rezult.txt", FileMode.Create);
            StreamWriter wt = new StreamWriter(fs);
            int n = dataGridView3.RowCount;
            int m = dataGridView3.ColumnCount;
            try
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < m; j++)
                    {

                        wt.Write(Convert.ToInt32(dataGridView3.Rows[i].Cells[j].Value) + " ");
                    }
                    wt.WriteLine();
                }
                wt.WriteLine();
                wt.Close();
                fs.Close();

                MessageBox.Show("Файл успешно сохранён в папке kursovik_Fil\\bin\\Debug\\rezult.txt");
            }
            catch
            {
                MessageBox.Show("Ошибка сохранения файла!");
            }
        }

        public void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {

        }
    }
}
