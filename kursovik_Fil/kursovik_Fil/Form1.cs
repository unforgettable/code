﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kursovik_Fil
{
    public partial class Form1 : Form
    {
        Form2 f2 = new Form2();
        public Form1()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if(Form1.dataGridView1.RowCount == 0 || Form1.dataGridView1.ColumnCount == 0)
            {
                MessageBox.Show("Заполните матрицу А");
                return;
            }
            FormMini mini = new FormMini();
            mini.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || Convert.ToInt32(textBox1.Text) <= 0)
            {
                MessageBox.Show("Заполните количество строк матрицы А");
                return;
            }
            if (textBox2.Text == "" || Convert.ToInt32(textBox2.Text) <= 0)
            {
                MessageBox.Show("Заполните количество столбцов матрицы А");
                return;
            }
            int doo,ot = doo = 0;
            Random rand = new Random();
            if (textBox6.Text != "" && textBox7.Text != "")
            {
                ot = Convert.ToInt32(textBox6.Text);
                doo = Convert.ToInt32(textBox7.Text);
            }
            if (doo < ot)
            {
                MessageBox.Show("ОТ > ДО");
                return;
            }
            int n = Convert.ToInt32(textBox1.Text);
            int m = Convert.ToInt32(textBox2.Text);
            Form1.dataGridView1.RowCount = n;
            Form1.dataGridView1.ColumnCount = m;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    if (textBox6.Text == "" || textBox7.Text == "")
                        Form1.dataGridView1.Rows[i].Cells[j].Value = rand.Next(0, 10);
                    else
                        Form1.dataGridView1.Rows[i].Cells[j].Value = rand.Next(ot, doo);
                }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            textBox6.MaxLength = 7;
            textBox7.MaxLength = 7;
            textBox8.MaxLength = 7;
            textBox9.MaxLength = 7;
        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void обАвтореToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Филимонов Олег ИП-16-3");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Form1.dataGridView1.RowCount == 0 || Form1.dataGridView1.ColumnCount == 0)
            {
                MessageBox.Show("Заполните матрицу А");
                return;
            }
            if (dataGridView2.RowCount == 0 || dataGridView2.ColumnCount == 0)
            {
                MessageBox.Show("Заполните матрицу В");
                return;
            }
            if (Form1.dataGridView1.RowCount != dataGridView2.RowCount || Form1.dataGridView1.ColumnCount != dataGridView2.ColumnCount)
            {
                MessageBox.Show("Матрицы должны быть одинаковой размерности.");
                return;
            }
            Form2 f2 = new Form2();
                f2.Show();
                f2.label2.Text = "Операция выполнена так:" + "\n" + "Аm×n + Bm×n = Cm×n" + "\n" + "Каждый элемент искомой матрицы равен сумме соответствующих элементов заданных матриц:" + "\n" + "cij = aij + bij";
                int n = Form1.dataGridView1.RowCount;
                int m = Form1.dataGridView1.ColumnCount;
                f2.dataGridView3.RowCount = n;
                f2.dataGridView3.ColumnCount = m;
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < m; j++)
                    {
                         f2.dataGridView3.Rows[i].Cells[j].Value = Convert.ToInt32(Form1.dataGridView1.Rows[i].Cells[j].Value) + Convert.ToInt32(dataGridView2.Rows[i].Cells[j].Value);
                    }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Form1.dataGridView1.RowCount == 0 || Form1.dataGridView1.ColumnCount == 0)
            {
                MessageBox.Show("Заполните матрицу А");
                return;
            }
            if (dataGridView2.RowCount == 0 || dataGridView2.ColumnCount == 0)
            {
                MessageBox.Show("Заполните матрицу В");
                return;
            }
            if (Form1.dataGridView1.RowCount != dataGridView2.RowCount || Form1.dataGridView1.ColumnCount != dataGridView2.ColumnCount)
            {
                MessageBox.Show("Матрицы должны быть одинаковой размерности.");
                return;
            }
            Form2 f2 = new Form2();
                f2.Show();
                f2.label2.Text = "Операция выполнена так:" + "\n" + "Аm×n - Bm×n = Cm×n" + "\n" + "Вычитание матриц (разность матриц) A - B есть операция вычисления матрицы C," + "\n" + "все элементы которой равны попарной разности всех соответствующих элементов матриц A и B," + "\n" + "то есть каждый элемент матрицы C равен:" + "\n" + "cij = aij - bij";
            int n = Form1.dataGridView1.RowCount;
            int m = Form1.dataGridView1.ColumnCount;
            f2.dataGridView3.RowCount = n;
            f2.dataGridView3.ColumnCount = m;
            for (int i = 0; i < n; i++)
                    for (int j = 0; j < m; j++)
                    {
                        f2.dataGridView3.Rows[i].Cells[j].Value = Convert.ToInt32(Form1.dataGridView1.Rows[i].Cells[j].Value) - Convert.ToInt32(dataGridView2.Rows[i].Cells[j].Value);
                    }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Form1.dataGridView1.RowCount ==  0 || Form1.dataGridView1.ColumnCount == 0)
            {
                MessageBox.Show("Заполните матрицу А");
                return;
            }
            if (dataGridView2.RowCount == 0 || dataGridView2.ColumnCount == 0)
            {
                MessageBox.Show("Заполните матрицу В");
                return;
            }
            if (Form1.dataGridView1.RowCount != dataGridView2.ColumnCount || dataGridView2.RowCount != Form1.dataGridView1.ColumnCount)
            {
                MessageBox.Show("Размерность матриц должна быть A(MxN) и B(NxM)");
                return;
            }
                Form2 f2 = new Form2();
                f2.Show();
                int n = Form1.dataGridView1.RowCount;
                int m = dataGridView2.ColumnCount;
                f2.dataGridView3.RowCount = n;
                f2.dataGridView3.ColumnCount = m;   

                int[,] A = new int[Form1.dataGridView1.RowCount, Form1.dataGridView1.ColumnCount];
                int[,] B = new int[dataGridView2.RowCount, dataGridView2.ColumnCount];

                for (int i = 0; i < Form1.dataGridView1.ColumnCount; i++)
                    for (int j = 0; j < Form1.dataGridView1.RowCount; j++)
                        A[j, i] = Convert.ToInt32(Form1.dataGridView1[i, j].Value);
                for (int i = 0; i < dataGridView2.ColumnCount; i++)
                    for (int j = 0; j < dataGridView2.RowCount; j++)
                        B[j, i] = Convert.ToInt32(dataGridView2[i, j].Value);

                for (int i = 0; i < Form1.dataGridView1.RowCount; i++)
                {
                    for (int j = 0; j < dataGridView2.ColumnCount; j++)
                    {
                        int s = 0;
                        for (int k = 0; k < dataGridView2.RowCount; k++)
                            s += A[i, k] * B[k, j];
                        f2.dataGridView3[j, i].Value = s;
                    }
                }
                f2.label2.Text = "Операция выполнена так:" + "\n" + "Результатом умножения матриц A(m×n) и B(n×m) будет матрица C(m×n) такая, \nчто элемент матрицы C, стоящий в i-той строке и j-том столбце (cij), \nравен сумме произведений элементов i-той строки матрицы A \nна соответствующие элементы j-того столбца матрицы B:\nc(i,j) = a(i,1) · b(1,j) + a(i,2) · b(2,j) + ... + a(i,n) · b(n,j)";
        }

        public void button6_Click(object sender, EventArgs e)
        {
            int n = Form1.dataGridView1.RowCount;
            int m = Form1.dataGridView1.ColumnCount;
            if (Form1.dataGridView1.RowCount != Form1.dataGridView1.ColumnCount)
            {
                MessageBox.Show("Матрицa A должнa быть квадратнaя.");
                return;
            }
            if (Form1.dataGridView1.RowCount == 0 || Form1.dataGridView1.ColumnCount == 0)
            {
                MessageBox.Show("Заполните матрицу А");
                return;
            }
            if (Form1.dataGridView1.RowCount == Form1.dataGridView1.ColumnCount)
            {
                double [,] matrix = new double[n, m];
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < m; j++)
                    {
                        matrix[i, j] = Convert.ToInt32(Form1.dataGridView1.Rows[i].Cells[j].Value);
                    }
                MessageBox.Show(Convert.ToString(Determ(matrix)));
            }


        }
        public static double Determ(double[,] matrix)
        {
            double det = 0;
            int Rank = matrix.GetLength(0);
            if (Rank == 1) det = matrix[0, 0];
            if (Rank == 2) det = matrix[0, 0] * matrix[1, 1] - matrix[0, 1] * matrix[1, 0];
            if (Rank > 2)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    det += Math.Pow(-1, 0 + j) * matrix[0, j] * Determ(GetMinor(matrix, 0, j));
                }
            }
            return det;
        }
        public static double[,] GetMinor(double[,] matrix, int row, int column)
        {
            double[,] buf = new double[matrix.GetLength(0) - 1, matrix.GetLength(0) - 1];
            for (int i = 0; i < matrix.GetLength(0); i++)
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if ((i != row) || (j != column))
                    {
                        if (i > row && j < column) buf[i - 1, j] = matrix[i, j];
                        if (i < row && j > column) buf[i, j - 1] = matrix[i, j];
                        if (i > row && j > column) buf[i - 1, j - 1] = matrix[i, j];
                        if (i < row && j < column) buf[i, j] = matrix[i, j];
                    }
                }
            return buf;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Form2 f2 = new Form2();
            int n = Form1.dataGridView1.RowCount;
            int m = Form1.dataGridView1.ColumnCount;
            f2.dataGridView3.RowCount = m;
            f2.dataGridView3.ColumnCount = n;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    f2.dataGridView3.Rows[j].Cells[i].Value = Form1.dataGridView1.Rows[i].Cells[j].Value;
                }
            f2.label2.Text = "Операция выполнена так:" + "\n" + "Транспонированая матрица получается из исходной \n путем замены строк столбцами c одинаковыми номерами.";
            f2.Show();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (!string.IsNullOrEmpty(textBox1.Text) && e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (!string.IsNullOrEmpty(textBox4.Text) && e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (!string.IsNullOrEmpty(textBox2.Text) && e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (!string.IsNullOrEmpty(textBox3.Text) && e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || ((string.IsNullOrEmpty(textBox6.Text) && e.KeyChar == '-')) || (e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || ((string.IsNullOrEmpty(textBox8.Text) && e.KeyChar == '-')) || (e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void textBox9_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || ((string.IsNullOrEmpty(textBox9.Text) && e.KeyChar == '-')) || (e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || ((string.IsNullOrEmpty(textBox7.Text) && e.KeyChar == '-')) || (e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (textBox3.Text == "" || Convert.ToInt32(textBox3.Text) <= 0)
            {
                MessageBox.Show("Заполните количество строк матрицы B");
                return;
            }
            if (textBox4.Text == "" || Convert.ToInt32(textBox4.Text) <= 0)
            {
                MessageBox.Show("Заполните количество столбцов матрицы B");
                return;
            }
            int doo, ot = doo = 0;
            Random rand = new Random();
            if (textBox9.Text != "" && textBox8.Text != "")
            {
                ot = Convert.ToInt32(textBox9.Text);
                doo = Convert.ToInt32(textBox8.Text);
            }
            if (doo < ot)
            {
                MessageBox.Show("ОТ > ДО");
                return;
            }
            if (textBox3.Text != "" && textBox4.Text != "")
            {
                int n = Convert.ToInt32(textBox3.Text);
                int m = Convert.ToInt32(textBox4.Text);
                dataGridView2.RowCount = n;
                dataGridView2.ColumnCount = m;
                for (int i = 0; i < n; i++)
                    for (int j = 0; j < m; j++)
                    {
                        if (textBox9.Text == "" && textBox8.Text == "")
                            dataGridView2.Rows[i].Cells[j].Value = rand.Next(0, 10);
                        else
                            dataGridView2.Rows[i].Cells[j].Value = rand.Next(ot, doo);
                    }
            }
        }

        private void dataGridView2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) ||  (e.KeyChar == '-') || (e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void dataGridView2_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox tb = (TextBox)e.Control;
            tb.KeyPress += new KeyPressEventHandler(dataGridView2_KeyPress);
        }

        private void dataGridView1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || (e.KeyChar == '-') || (e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox tb = (TextBox)e.Control;
            tb.KeyPress += new KeyPressEventHandler(dataGridView1_KeyPress);
        }
    }
}
