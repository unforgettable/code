﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kursovik_Fil
{
    public partial class FormMini : Form
    {
        public FormMini()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "")
            {
                MessageBox.Show("Заполните поле 'число'");
                return;
            }
            //Form1 f1 = new Form1();
            int l = Convert.ToInt32(textBox1.Text);
            Form2 f2 = new Form2();
            this.Hide();
            f2.Show();
            int n = Form1.dataGridView1.RowCount;
            int m = Form1.dataGridView1.ColumnCount;
            f2.dataGridView3.RowCount = n;
            f2.dataGridView3.ColumnCount = m;
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    f2.dataGridView3.Rows[i].Cells[j].Value = Convert.ToInt32(Form1.dataGridView1.Rows[i].Cells[j].Value) * l;
                }
            f2.label2.Text = "Операция выполнена так:" + "\n" + "b(i,j) = k · a(i,j)";
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsNumber(e.KeyChar) || ((string.IsNullOrEmpty(textBox1.Text) && e.KeyChar == '-')) || (e.KeyChar == 8))
            {
                return;
            }

            e.Handled = true;
        }

        private void FormMini_Load(object sender, EventArgs e)
        {
            textBox1.MaxLength = 8;
        }
    }
}
