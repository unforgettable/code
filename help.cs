﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


//Пример кода для вывода на экран префаба.

public class help : MonoBehaviour
{
    public Button Btn_Help;
    public Vector3 help_position = new Vector3(Screen.width / 2, Screen.height / 2, 0);
    public GameObject help_obj;
    bool is_help_create;

    void Start()
    {
        is_help_create = false;
        Btn_Help.GetComponent<Button>().onClick.AddListener(delegate
        {
            Heeeelp();
        });
    }

    void Heeeelp()
    {
        is_help_create = true;
        Instantiate(help_obj, GameObject.Find("Help_position").transform, false);      //Создание объекта объявления
        Transform objError = GameObject.Find("Help_pref(Clone)").transform.GetChild(2);
        objError.GetComponent<Text>().text = PlayerPrefs.GetString("Help");   //Ставится текст исходя из языка 

        //Ошибка создана
    }

    void FixedUpdate()
    {
        //Если нажимаем на мышь и ошибка выведена, то удаялем ошибку с экрана
        if (Input.GetMouseButtonDown(0) && is_help_create)
        {
            is_help_create = false;
            Destroy(GameObject.Find("Help_pref" + "(Clone)"));
            Camera.main.GetComponent<CheckButtonClick>().BtnStart.GetComponent<Button>().interactable = true;
        }
    }
}
